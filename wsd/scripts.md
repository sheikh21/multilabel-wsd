- Train your model

!python3 multilabel-wsd/wsd/train.py --name bert-large \
    --language_model bert-large-cased \
    --train_path multilabel-wsd/data/preprocessed/glosses/semcor.glosses.untagged.json \
    --dev_path multilabel-wsd/data/preprocessed/semeval2007/semeval2007.json \
    --use_graph_convolution \
    --use_trainable_graph \
    --include_hyponyms \
    --include_hypernyms \
    --include_pagerank \
    --pagerank_k 20
    # --include_similar \
    # --include_related \
    # --include_also_see \
    # --include_verb_groups \

- Predict output

!python3 multilabel-wsd/wsd/predict.py --processor /content/multilabel-wsd/checkpoints/bert-large/processor_config.json \
    --model /content/multilabel-wsd/checkpoints/bert-large/checkpoint_val_f1=0.7011_epoch=008.ckpt \
    --model_input /content/multilabel-wsd/data/preprocessed/all/all.json \
    --model_output /content/multilabel-wsd/output.txt

- Evaluation

!python3 multilabel-wsd/wsd/evaluate.py --processor /content/multilabel-wsd/checkpoints/bert-large/processor_config.json \
    --model /content/multilabel-wsd/checkpoints/bert-large/checkpoint_val_f1=0.7011_epoch=008.ckpt \
    --model_input /content/multilabel-wsd/data/preprocessed/all/all.json \
    --model_output /content/multilabel-wsd/output.gold.txt \
    --evaluation_input /content/multilabel-wsd/data/original/all/ALL.gold.key.txt