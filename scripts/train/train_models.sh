#!/bin/bash

# Semcor + relations
python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see

python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see \
    --include_hypernyms

python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see \
    --include_hyponyms

python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see \
    --include_hypernyms \
    --include_hyponyms

python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see \
    --include_hypernyms \
    --include_hyponyms \
    --include_instance_hypernyms \
    --include_instance_hyponyms

# python3 wsd/train.py --name bert-large --language_model bert-large-cased \
#     --include_similar \
#     --include_related \
#     --include_verb_groups \
#     --include_also_see \
#     --include_hypernyms \
#     --include_hyponyms \
#     --include_pagerank \
#     --pagerank_k 20

# Semcor + untagged glosses
# python3 wsd/train.py --name bert-large --language_model bert-large-cased \
#     --train_path data/preprocessed/glosses/semcor.glosses.untagged.json \
#     --include_similar \
#     --include_related \
#     --include_verb_groups \
#     --include_hypernyms \
#     --include_hyponyms \
#     --include_also_see \
#     --include_pagerank \
#     --pagerank_k 20

# Semcor + tagged glosses + examples
python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --train_path data/preprocessed/glosses/semcor.glosses.examples.json \
    --include_similar \
    --include_related \
    --include_verb_groups \
    --include_also_see \
    --include_hypernyms \
    --include_hyponyms

# Baselines
python3 wsd/train.py --name bert-large --language_model bert-large-cased --loss_type cross_entropy

python3 wsd/train.py --name bert-large --language_model bert-large-cased

# python3 wsd/train.py --name bert-large --language_model bert-large-cased \
#     --train_path data/preprocessed/glosses/semcor.glosses.untagged.json

python3 wsd/train.py --name bert-large --language_model bert-large-cased \
    --train_path data/preprocessed/glosses/semcor.glosses.examples.json